import math
import pygame
import mesh as mesh

from random import shuffle
from config import Config


class StupidPlayer2:

    def __init__(self, display, display_settings, mesh, bomb_type, position, is_black, load_images, game):
        self.display = display
        self.display_settings = display_settings
        self.mesh = mesh
        self.bomb_type = bomb_type
        self.is_black = is_black
        self.game = game
        (self.x, self.y) = position
        self.speed = 3.5
        self.allowed_bombs = 2
        self.frame = 0
        self.direction = mesh.none
        if load_images:
            self.load_images(is_black)

        self.target_path = []
        self.target_min_dist = 0.25
        self.target_selected = False
        self.last_direction = mesh.none
        self.bomb_range, self.enemy_range = False, False

    def update(self, dt, _):
        self.bomb_planted = False

        (self.direction, plant_bomb) = self.select_ai_direction()

        if (self.direction != mesh.none):
            actual_pos =  (self.x, self.y)
            (self.x, self.y) = self.mesh.update_position((self.x, self.y, self.speed), self.direction, dt, [self.bomb_type])
            if (not self.bomb_range and not self.enemy_range and (self.is_in_bomb_range()[0] or self.is_in_enemy_range()[0])):
                self.direction = mesh.none
                self.target_selected = False
            elif not self.enemy_range:
                for i in range(len(self.game.sprites) - 2):
                    x, y = self.game.sprites[2 + i].get_next_position(dt * 10)
                    if math.sqrt((self.x - x) ** 2 + (self.y - y) ** 2) < 1:
                        self.direction = mesh.none
                        self.target_selected = False
                        break

            (self.x, self.y) = actual_pos


        if plant_bomb and self.mesh.put_bomb((self.x, self.y), self):
            self.allowed_bombs -= 1
            self.bomb_planted = True

        if self.direction is not mesh.none:
            (self.x, self.y) = self.mesh.update_position(
                (self.x, self.y, self.speed), self.direction, dt, [self.bomb_type])

    def release_bomb(self):
        self.allowed_bombs += 1

    def draw(self):
        self.frame += 1
        image = self.img_idle

        if self.direction is not mesh.none:
            image = self.images[self.direction][self.frame // 10]

        if self.frame == 29:
            self.frame = 0

        (ratio, padding) = self.display_settings
        self.display.blit(
            image, (self.x * ratio + padding, self.y * ratio + padding))

    def draw_learning(self):
        (ratio, padding) = self.display_settings
        color = [0, 0, 0] if self.is_black else [51, 51, 51]
        pygame.draw.rect(self.display, color,
                         [self.x * ratio + padding, self.y * ratio + padding, ratio, ratio], 0)

    def draw_to_buffer(self, buffer):
        color = 0 if self.is_black else 51
        for i in range(8):
            for j in range(8):
                buffer[int(self.y * 8 + i)][int(self.x * 8 + j)] = color

    def draw_visualisation(self):
        pad_x, pad_y = Config['game']['width'] - 325, Config['game']['height'] - 325
        pygame.draw.rect(self.display, [50, 50, 50], [pad_x, pad_y, 325, 325], 0)
        for (x, y) in self.target_path:
            pygame.draw.rect(self.display, [2, 184, 117], [9 + pad_x + 25 * x, 9 + pad_y + 25 * y, 7, 7], 0)
        pygame.draw.rect(self.display, [50, 50, 240], [pad_x + 25 * self.x, pad_y + 25 * self.y, 25, 25], 0)
        enemy = self.game.sprites[0] if self.is_black else self.game.sprites[1]
        pygame.draw.rect(self.display, [140, 140, 240], [pad_x + 25 * enemy.x, pad_y + 25 * enemy.y, 25, 25], 0)
        for i in range(len(self.game.sprites) - 2):
            pygame.draw.rect(self.display, [250, 0, 190], [pad_x + 25 * self.game.sprites[i + 2].x, pad_y + 25 * self.game.sprites[i + 2].y, 25, 25], 0)
        for (x, row) in enumerate(self.mesh.m):
            for (y, el) in enumerate(row):
                if (el in mesh.bombs_fields):
                    color = [240, 50, 50]
                elif (el in mesh.explosion_fields):
                    color = [255, 255, 0]
                elif (el == mesh.wall):
                    color = [240, 240, 240]
                else:
                    color = [230, 110, 0]
                if (el != mesh.empty):
                    pygame.draw.rect(self.display, color, [pad_x + 25 * x, pad_y + 25 * y, 25, 25], 0)
        if self.bomb_range:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'], 0, 325, 100], 0)
        if self.enemy_range:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'], 100, 325, 100], 0)
        if self.last_direction == mesh.up:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'], 200, 325, 100], 0)
        if self.last_direction == mesh.down:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'], 407, 325, 100], 0)
        if self.last_direction == mesh.left:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'], 300, 162, 107], 0)
        if self.last_direction == mesh.right:
            pygame.draw.rect(self.display, [255, 255, 0], [Config['game']['height'] + 162, 300, 162, 107], 0)
        self.display.blit(self.images['enemy'], (Config['game']['height'] + 162 - 32, 150 - 32))
        self.display.blit(self.images['bomb'], (Config['game']['height'] + 162 - 32, 50 - 32))
        self.display.blit(self.images['u'], (Config['game']['height'] + 162 - 32, 250 - 32))
        self.display.blit(self.images['d'], (Config['game']['height'] + 162 - 32, 457 - 32))
        self.display.blit(self.images['l'], (Config['game']['height'] + 81 - 32, 350 - 32))
        self.display.blit(self.images['r'], (Config['game']['height'] + 81 + 162 - 32, 350 - 32))
        pygame.draw.line(self.display, [50, 50, 50], (Config['game']['height'], 100), (Config['game']['width'], 100))
        pygame.draw.line(self.display, [50, 50, 50], (Config['game']['height'], 200), (Config['game']['width'], 200))
        pygame.draw.line(self.display, [50, 50, 50], (Config['game']['height'], 300), (Config['game']['width'], 300))
        pygame.draw.line(self.display, [50, 50, 50], (Config['game']['height'] + 162, 300), (Config['game']['height'] + 162, 407))
        pygame.draw.line(self.display, [50, 50, 50], (Config['game']['height'], 407), (Config['game']['width'], 407))

    def distance_to(self, sprite):
        return math.sqrt((self.x - sprite.x) ** 2 + (self.y - sprite.y) ** 2)

    def load_images(self, is_black):
        self.images = {}
        
        self.images['enemy'] = pygame.image.load('./assets/enemy1.png').convert_alpha()
        self.images['bomb'] = pygame.image.load('./assets/bomb_1.png').convert_alpha()

        self.images['l'] = pygame.image.load('./assets/left.png').convert_alpha()
        self.images['r'] = pygame.image.load('./assets/right.png').convert_alpha()
        self.images['u'] = pygame.image.load('./assets/up.png').convert_alpha()
        self.images['d'] = pygame.image.load('./assets/down.png').convert_alpha()

        if not is_black:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1.png').convert_alpha(),
                pygame.image.load('./assets/player_idle.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1.png').convert_alpha(),
                pygame.image.load('./assets/player_up.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2.png').convert_alpha(),
                pygame.image.load('./assets/player_left.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2.png').convert_alpha(),
                pygame.image.load('./assets/player_right.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3.png').convert_alpha()]
        else:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_idleb.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2b.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_upb.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2b.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2b.png').convert_alpha(),
                pygame.image.load('./assets/player_leftb.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3b.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2b.png').convert_alpha(),
                pygame.image.load(
                    './assets/player_rightb.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3b.png').convert_alpha()]

    def select_ai_direction(self):
        direction, plant_bomb = mesh.none, False
        is_in_bomb_range, is_in_enemy_range = self.is_in_bomb_range(), self.is_in_enemy_range()
        distance_to_target = math.sqrt((self.x - self.target_path[-1][0]) ** 2 + (
            self.y - self.target_path[-1][1]) ** 2) if self.target_selected else -1
        actual_cell = (int(self.x) if abs(int(self.x) - self.x) <= 0.5 else int(self.x) + 1,
                      int(self.y) if abs(int(self.y) - self.y) <= 0.5 else int(self.y) + 1) 

        self.bomb_range, self.enemy_range = is_in_bomb_range[0], is_in_enemy_range[0]

        if is_in_enemy_range[0] or is_in_bomb_range[0]:
            self.target_path = self.get_target(actual_cell, is_in_bomb_range[0], is_in_enemy_range[0], is_in_bomb_range[1], is_in_enemy_range[1], None)
        elif not self.target_selected or distance_to_target < self.target_min_dist:
            if self.target_selected and self.allowed_bombs > 0:
                plant_bomb = True
            if self.mesh.bricks_count <= 10:
                enemy = self.game.sprites[0] if self.is_black else self.game.sprites[1]
                x, y = enemy.x, enemy.y
                target = (int(x) if abs(int(x) - x) <= 0.5 else int(x) + 1,
                        int(y) if abs(int(y) - y) <= 0.5 else int(y) + 1)
                self.target_path = self.get_target(actual_cell, is_in_bomb_range[0], is_in_enemy_range[0], is_in_bomb_range[1], is_in_enemy_range[1], target)
            else:
                self.target_path = self.get_target(actual_cell, is_in_bomb_range[0], is_in_enemy_range[0], is_in_bomb_range[1], is_in_enemy_range[1], None)

        direction = self.get_direction_to_target(actual_cell)
        self.target_selected = True
        self.last_direction = direction

        return direction, plant_bomb

    def is_in_bomb_range(self):
        return self.mesh.in_bomb_range_field(self)

    def is_in_enemy_range(self):
        cells = []
        in_range = False
        for i in range(len(self.game.sprites) - 2):
            x, y = self.game.sprites[2 + i].x, self.game.sprites[2 + i].y
            cell = (int(x) if abs(int(x) - x) <= 0.5 else int(x) + 1,
                    int(y) if abs(int(y) - y) <= 0.5 else int(y) + 1)
            cells.append(cell)
            if self.distance_to(self.game.sprites[2 + i]) < 1:
                in_range = True
        return (in_range, cells)
    
    def get_target(self, start_cell, is_in_bomb_range, is_in_enemy_range, explosion_cells, enemy_cells, target):
        prev = [[None for _ in range(mesh.size)] for _ in range(mesh.size)]
        dist = [[100000 for _ in range(mesh.size)] for _ in range(mesh.size)]
        dist[start_cell[0]][start_cell[1]] = 0
        queue = [(start_cell, 0)]
        
        while len(queue) > 0:
            (cell, distance) = queue.pop()
            neighbors = self.get_cell_neighbors(cell, enemy_cells)
            for neighbor in neighbors:
                weight = self.get_weight(neighbor, explosion_cells)
                alt = weight + distance
                neighbor_dist = dist[neighbor[0]][neighbor[1]]
                if alt < neighbor_dist:
                    prev[neighbor[0]][neighbor[1]] = cell
                    dist[neighbor[0]][neighbor[1]] = alt
                    queue.append((neighbor, alt))

        good_cells = [(x, y) for x in range(mesh.size) for y in range(mesh.size) if dist[x][y] < 100000]
        shuffle(good_cells)
        if is_in_bomb_range or is_in_enemy_range:
            cell, distance = None, None
            for x in range(mesh.size):
                for y in range(mesh.size):
                    if ((not any(c[0] == x and c[1] == y for c in explosion_cells)) and (distance is None or dist[x][y] < distance)):
                        cell = (x, y)
                        distance = dist[x][y]
            if (cell is None):
                cell = good_cells[0]
        elif target is not None and any(c[0] == target[0] and c[1] == target[1] for c in good_cells):
            cell = target
        else:
            cell = good_cells[0]

        path = [cell]
        while prev[cell[0]][cell[1]] is not None:
            cell = prev[cell[0]][cell[1]]
            path.insert(0, cell)
        return path

    def get_cell_neighbors(self, cell, enemy_cells):
        good_fields = [mesh.empty, self.bomb_type]
        neighbors = []

        if cell[0] > 0:
            neighbors.append((cell[0] - 1, cell[1]))

        if cell[1] > 0:
            neighbors.append((cell[0], cell[1] - 1))

        if cell[0] < mesh.size - 1:
            neighbors.append((cell[0] + 1, cell[1]))

        if cell[1] < mesh.size - 1:
            neighbors.append((cell[0], cell[1] + 1))

        return [n for n in neighbors if self.mesh.get(n) in good_fields and not any(c[0] == n[0] and c[1] == n[1] for c in enemy_cells)]

    def get_weight(self, cell, explosion_cells):
        return 3 if self.is_in_explosion_range(cell, explosion_cells) else 1

    def is_in_explosion_range(self, cell, explosion_cells):
        return any(cell[0] == e[0] and cell[1] == e[1] for e in explosion_cells)

    def get_direction_to_target(self, actual_cell):
        direction = mesh.none
        last_index = len(self.target_path) - 1
        for (i, target) in enumerate(self.target_path):
            if actual_cell[0] == target[0] and actual_cell[1] == target[1]:
                if i == last_index:
                    if self.x > target[0]:
                        direction = mesh.left
                    if self.x < target[0]:
                        direction = mesh.right
                    if self.y > target[1]:
                        direction = mesh.up
                    if self.y < target[1]:
                        direction = mesh.down
                else:
                    next_target = self.target_path[i + 1]
                    if next_target[0] > target[0]:
                        direction = mesh.right
                    if next_target[0] < target[0]:
                        direction = mesh.left
                    if next_target[1] > target[1]:
                        direction = mesh.down
                    if next_target[1] < target[1]:
                        direction = mesh.up
                break

        return direction