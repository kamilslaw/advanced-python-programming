import pygame
import mesh as mesh
import random


class SimpleEnemy:

    def __init__(self, display, display_settings, mesh, load_images):
        self.display = display
        self.display_settings = display_settings
        self.mesh = mesh
        self.init_position()
        self.speed = 1
        if load_images:
            self.image = pygame.image.load('./assets/enemy1.png').convert_alpha()

    def update(self, dt, keys):
        possible_directions = self.mesh.get_possible_directions(
            (self.x, self.y), [])

        if not possible_directions:
            self.direction = mesh.none
        elif self.direction not in possible_directions:
            self.direction = random.choice(possible_directions)

        (self.x, self.y) = self.mesh.update_position(
            (self.x, self.y, self.speed), self.direction, dt, [])

    def get_next_position(self, dt):
        next_pos = self.mesh.update_position((self.x, self.y, self.speed), self.direction, dt, [])
        return next_pos

    def draw(self):
        (ratio, padding) = self.display_settings
        self.display.blit(
            self.image, (self.x * ratio + padding, self.y * ratio + padding))
    
    def draw_learning(self):
        (ratio, padding) = self.display_settings
        pygame.draw.rect(self.display, [131, 131, 131],
                         [self.x * ratio + padding, self.y * ratio + padding, ratio, ratio], 0)
    
    def draw_to_buffer(self, buffer):
        color = 131
        for i in range(8):
            for j in range(8):
                buffer[int(self.y * 8 + i)][int(self.x * 8 + j)] = color

    def init_position(self):
        self.direction = mesh.none
        (self.x, self.y) = random.choice(
            [(x, y) for x in range(2, mesh.size - 1) for y in range(2, mesh.size - 1) if self.mesh.m[x][y] is mesh.empty])
