import math
import pygame
import mesh as mesh


class Player:

    def __init__(self, display, display_settings, mesh, bomb_type, position, is_black, load_images, keys):
        self.display = display
        self.display_settings = display_settings
        self.mesh = mesh
        self.bomb_type = bomb_type
        self.keys = keys
        self.is_black = is_black
        (self.x, self.y) = position
        self.speed = 3.5
        self.allowed_bombs = 3
        self.frame = 0
        self.direction = mesh.none
        if load_images:
            self.load_images(is_black)

    def update(self, dt, keys):
        self.bomb_planted = False
        possible_directions = self.mesh.get_possible_directions(
            (self.x, self.y), [self.bomb_type])

        self.direction = mesh.none
        if mesh.left in possible_directions and keys[self.keys['left']]:
            self.direction = mesh.left
        if mesh.right in possible_directions and keys[self.keys['right']]:
            self.direction = mesh.right
        if mesh.up in possible_directions and keys[self.keys['up']]:
            self.direction = mesh.up
        if mesh.down in possible_directions and keys[self.keys['down']]:
            self.direction = mesh.down
        if self.allowed_bombs > 0 and keys[self.keys['bomb']] and self.mesh.put_bomb((self.x, self.y), self):
            self.allowed_bombs -= 1
            self.bomb_planted = True

        if self.direction is not mesh.none:
            (self.x, self.y) = self.mesh.update_position(
                (self.x, self.y, self.speed), self.direction, dt, [self.bomb_type])

    def release_bomb(self):
        self.allowed_bombs += 1

    def draw(self):
        self.frame += 1
        image = self.img_idle

        if self.direction is not mesh.none:
            image = self.images[self.direction][self.frame // 10]

        if self.frame == 29:
            self.frame = 0

        (ratio, padding) = self.display_settings
        self.display.blit(
            image, (self.x * ratio + padding, self.y * ratio + padding))

    def draw_learning(self):
        (ratio, padding) = self.display_settings
        color = [0, 0, 0] if self.is_black else [51, 51, 51]
        pygame.draw.rect(self.display, color,
                         [self.x * ratio + padding, self.y * ratio + padding, ratio, ratio], 0)
    
    def draw_to_buffer(self, buffer):
        color = 0 if self.is_black else 51
        for i in range(8):
            for j in range(8):
                buffer[int(self.y * 8 + i)][int(self.x * 8 + j)] = color

    def distance_to(self, sprite):
        return math.sqrt((self.x - sprite.x) ** 2 + (self.y - sprite.y) ** 2)

    def load_images(self, is_black):
        self.images = {}

        if not is_black:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1.png').convert_alpha(),
                pygame.image.load('./assets/player_idle.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1.png').convert_alpha(),
                pygame.image.load('./assets/player_up.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2.png').convert_alpha(),
                pygame.image.load('./assets/player_left.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2.png').convert_alpha(),
                pygame.image.load('./assets/player_right.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3.png').convert_alpha()]
        else:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_idleb.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2b.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_upb.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2b.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2b.png').convert_alpha(),
                pygame.image.load('./assets/player_leftb.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3b.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2b.png').convert_alpha(),
                pygame.image.load(
                    './assets/player_rightb.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3b.png').convert_alpha()]
