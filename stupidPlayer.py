import math
import pygame
import mesh as mesh
from random import shuffle


class StupidPlayer:

    def __init__(self, display, display_settings, mesh, bomb_type, position, is_black, load_images, game):
        self.display = display
        self.display_settings = display_settings
        self.mesh = mesh
        self.bomb_type = bomb_type
        self.is_black = is_black
        self.game = game
        (self.x, self.y) = position
        self.speed = 3.5
        self.allowed_bombs = 1
        self.frame = 0
        if load_images:
            self.load_images(is_black)

        self.target = (-1, -1)
        self.target_min_dist = 0.25
        self.target_selected = False
        self.last_direction = None

    def update(self, dt, keys):
        self.bomb_planted = False
        possible_directions = self.mesh.get_possible_directions(
            (self.x, self.y), [self.bomb_type])

        (self.direction, plant_bomb) = self.select_ai_direction(possible_directions)

        if plant_bomb and self.mesh.put_bomb((self.x, self.y), self):
            self.allowed_bombs -= 1
            self.bomb_planted = True

        if self.direction is not mesh.none:
            (self.x, self.y) = self.mesh.update_position(
                (self.x, self.y, self.speed), self.direction, dt, [self.bomb_type])

    def release_bomb(self):
        self.allowed_bombs += 1

    def draw(self):
        self.frame += 1
        image = self.img_idle

        if self.direction is not mesh.none:
            image = self.images[self.direction][self.frame // 10]

        if self.frame == 29:
            self.frame = 0

        (ratio, padding) = self.display_settings
        self.display.blit(
            image, (self.x * ratio + padding, self.y * ratio + padding))

    def draw_learning(self):
        (ratio, padding) = self.display_settings
        color = [0, 0, 0] if self.is_black else [51, 51, 51]
        pygame.draw.rect(self.display, color,
                         [self.x * ratio + padding, self.y * ratio + padding, ratio, ratio], 0)

    def draw_to_buffer(self, buffer):
        color = 0 if self.is_black else 51
        for i in range(8):
            for j in range(8):
                buffer[int(self.y * 8 + i)][int(self.x * 8 + j)] = color

    def distance_to(self, sprite):
        return math.sqrt((self.x - sprite.x) ** 2 + (self.y - sprite.y) ** 2)

    def load_images(self, is_black):
        self.images = {}

        if not is_black:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1.png').convert_alpha(),
                pygame.image.load('./assets/player_idle.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1.png').convert_alpha(),
                pygame.image.load('./assets/player_up.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2.png').convert_alpha(),
                pygame.image.load('./assets/player_left.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2.png').convert_alpha(),
                pygame.image.load('./assets/player_right.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3.png').convert_alpha()]
        else:
            self.images[mesh.down] = [
                pygame.image.load(
                    './assets/player_down_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_idleb.png').convert_alpha(),
                pygame.image.load('./assets/player_down_2b.png').convert_alpha()]
            self.img_idle = self.images[mesh.down][1]

            self.images[mesh.up] = [
                pygame.image.load('./assets/player_up_1b.png').convert_alpha(),
                pygame.image.load('./assets/player_upb.png').convert_alpha(),
                pygame.image.load('./assets/player_up_2b.png').convert_alpha()]

            self.images[mesh.left] = [
                pygame.image.load(
                    './assets/player_left_2b.png').convert_alpha(),
                pygame.image.load('./assets/player_leftb.png').convert_alpha(),
                pygame.image.load('./assets/player_left_3b.png').convert_alpha()]

            self.images[mesh.right] = [
                pygame.image.load(
                    './assets/player_right_2b.png').convert_alpha(),
                pygame.image.load(
                    './assets/player_rightb.png').convert_alpha(),
                pygame.image.load('./assets/player_right_3b.png').convert_alpha()]

    def select_ai_direction(self, possible_directions):
        direction, plant_bomb = mesh.none, False
        is_in_bomb_range, is_in_enemy_range = self.is_in_bomb_range(), self.is_in_enemy_range()
        distance_to_target = math.sqrt((self.x - self.target[0]) ** 2 + (
            self.y - self.target[1]) ** 2) if self.target_selected else -1
        actual_cell = (int(self.x) if abs(int(self.x) - self.x) <= 0.5 else int(self.x) + 1,
                      int(self.y) if abs(int(self.y) - self.y) <= 0.5 else int(self.y) + 1)

        if is_in_enemy_range[0]:
            self.target = self.get_random_target(actual_cell, is_in_enemy_range[1])
        elif is_in_bomb_range[0]:
            self.target = self.get_random_target(actual_cell, is_in_bomb_range[1])
        elif not self.target_selected or distance_to_target < self.target_min_dist:
            if self.target_selected and self.allowed_bombs > 0:
                plant_bomb = True
            if self.mesh.bricks_count <= 5:
                self.target = (self.game.sprites[0].x, self.game.sprites[0].y)
            else:
                self.target = self.get_random_target(actual_cell, (1, 1))

        self.target_selected = True

        shuffle(possible_directions)
        if self.last_direction in possible_directions and self.can_be_reached(self.last_direction, actual_cell, self.target):
            direction = self.last_direction
        else:
            for d in possible_directions:
                if self.can_be_reached(d, actual_cell, self.target):
                    direction = d
                    break

        self.last_direction = direction
        return direction, plant_bomb

    def is_in_bomb_range(self):
        return self.mesh.in_bomb_range_field(self)

    def is_in_enemy_range(self):
        for i in range(len(self.game.sprites) - 2):
            if self.distance_to(self.game.sprites[2 + i]) < 1:
                x, y = self.game.sprites[2 + i].x, self.game.sprites[2 + i].y
                cell = (int(x) if abs(int(x) - x) <= 0.5 else int(x) + 1,
                      int(y) if abs(int(y) - y) <= 0.5 else int(y) + 1)
                return (True, cell)
        return (False, None)
    
    def get_random_target(self, start_cell, forbidden_cell):
        good_fields = [mesh.empty, self.bomb_type]
        cells = [0] * (mesh.size * mesh.size) # 0 - not visited, 1 - visited, 2 - forbidden
        prevs = [((1, 1), -1)] * (mesh.size * mesh.size)

        actual_cell = start_cell
        prevs[actual_cell[1] * mesh.size + actual_cell[0]] = (actual_cell, 0)
        cells[forbidden_cell[1] * mesh.size + forbidden_cell[0]] = 2

        start_cell_neighbors_indices = []
        if start_cell[0] > 0: start_cell_neighbors_indices.append((start_cell[0] - 1, start_cell[1]))
        if start_cell[1] > 0: start_cell_neighbors_indices.append((start_cell[0], start_cell[1] - 1))
        if start_cell[0] < mesh.size - 1: start_cell_neighbors_indices.append((start_cell[0] + 1, start_cell[1]))
        if start_cell[1] < mesh.size - 1: start_cell_neighbors_indices.append((start_cell[0], start_cell[1] + 1))
        
        depth = 1
        while any(cells[i[1] * mesh.size + i[0]] == 0 for i in start_cell_neighbors_indices):
            index = actual_cell[1] * mesh.size + actual_cell[0]
            cells[index] = 1
            any_selected = False
            if actual_cell[0] > 0 and cells[actual_cell[1] * mesh.size + actual_cell[0] - 1] == 0:
                new_index = actual_cell[1] * mesh.size + actual_cell[0] - 1
                if self.mesh.m[actual_cell[0] - 1][actual_cell[1]] in good_fields:
                    prevs[new_index] = (actual_cell, depth)
                    actual_cell = (actual_cell[0] - 1, actual_cell[1])
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[1] > 0 and cells[(actual_cell[1] - 1) * mesh.size + actual_cell[0]] == 0:
                new_index = (actual_cell[1] - 1) * mesh.size + actual_cell[0]
                if self.mesh.m[actual_cell[0]][actual_cell[1] - 1] in good_fields:
                    prevs[new_index] = (actual_cell, depth)
                    actual_cell = (actual_cell[0], actual_cell[1] - 1)
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[0] < mesh.size - 1 and cells[actual_cell[1] * mesh.size + actual_cell[0] + 1] == 0:
                new_index = actual_cell[1] * mesh.size + actual_cell[0] + 1
                if self.mesh.m[actual_cell[0] + 1][actual_cell[1]] in good_fields:
                    prevs[new_index] = (actual_cell, depth)
                    actual_cell = (actual_cell[0] + 1, actual_cell[1])
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[1] < mesh.size - 1 and cells[(actual_cell[1] + 1) * mesh.size + actual_cell[0]] == 0:
                new_index = (actual_cell[1] + 1) * mesh.size + actual_cell[0]
                if self.mesh.m[actual_cell[0]][actual_cell[1] + 1] in good_fields:
                    prevs[new_index] = (actual_cell, depth)
                    actual_cell = (actual_cell[0], actual_cell[1] + 1)
                    any_selected = True
                else:
                    cells[new_index] = 2

            if not any_selected:  # We go back
                actual_cell = prevs[index][0]
                depth -= 1
            else:
                depth += 1

        prevs.sort(key=lambda x: x[1], reverse=True)
        return prevs[0][0]
        # cells_indices = [(y * mesh.size + x, (x, y)) for x in range(mesh.size) for y in range(mesh.size)]
        # shuffle(cells_indices)
        # for (cell_index, position) in cells_indices:
        #     if cells[cell_index] == 1:
        #         return position
        
    def can_be_reached(self, direction, actual_cell, target_cell):
        if (actual_cell[0] == target_cell[0] and abs(actual_cell[1] - target_cell[1]) <= 1) or (actual_cell[1] == target_cell[1] and abs(actual_cell[0] - target_cell[0]) <= 1):
            return True

        cell = None

        if direction == mesh.left and actual_cell[0] > 0:
            cell = (actual_cell[0] - 1, actual_cell[1])

        if direction == mesh.right and actual_cell[0] < mesh.size - 1:
            cell = (actual_cell[0] + 1, actual_cell[1])

        if direction == mesh.up and actual_cell[1] > 0:
            cell = (actual_cell[0], actual_cell[1] - 1)

        if direction == mesh.down and actual_cell[1] < mesh.size - 1:
            cell = (actual_cell[0], actual_cell[1] + 1)
        
        return cell is not None and self.can_be_reached_it_is_duplicated_method(cell, actual_cell, target_cell)
            
    def can_be_reached_it_is_duplicated_method(self, start_cell, forbidden_cell, target_cell):
        good_fields = [mesh.empty, self.bomb_type]
        cells = [0] * (mesh.size * mesh.size) # 0 - not visited, 1 - visited, 2 - forbidden
        prevs = [-1] * (mesh.size * mesh.size)

        actual_cell = start_cell
        prevs[actual_cell[1] * mesh.size + actual_cell[0]] = actual_cell
        cells[forbidden_cell[1] * mesh.size + forbidden_cell[0]] = 2

        start_cell_neighbors_indices = []
        if start_cell[0] > 0: start_cell_neighbors_indices.append((start_cell[0] - 1, start_cell[1]))
        if start_cell[1] > 0: start_cell_neighbors_indices.append((start_cell[0], start_cell[1] - 1))
        if start_cell[0] < mesh.size - 1: start_cell_neighbors_indices.append((start_cell[0] + 1, start_cell[1]))
        if start_cell[1] < mesh.size - 1: start_cell_neighbors_indices.append((start_cell[0], start_cell[1] + 1))

        while any(cells[i[1] * mesh.size + i[0]] == 0 for i in start_cell_neighbors_indices):
            index = actual_cell[1] * mesh.size + actual_cell[0]
            cells[index] = 1
            any_selected = False
            if actual_cell[0] > 0 and cells[actual_cell[1] * mesh.size + actual_cell[0] - 1] == 0:
                new_index = actual_cell[1] * mesh.size + actual_cell[0] - 1
                if self.mesh.m[actual_cell[0] - 1][actual_cell[1]] in good_fields:
                    prevs[new_index] = actual_cell
                    actual_cell = (actual_cell[0] - 1, actual_cell[1])
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[1] > 0 and cells[(actual_cell[1] - 1) * mesh.size + actual_cell[0]] == 0:
                new_index = (actual_cell[1] - 1) * mesh.size + actual_cell[0]
                if self.mesh.m[actual_cell[0]][actual_cell[1] - 1] in good_fields:
                    prevs[new_index] = actual_cell
                    actual_cell = (actual_cell[0], actual_cell[1] - 1)
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[0] < mesh.size - 1 and cells[actual_cell[1] * mesh.size + actual_cell[0] + 1] == 0:
                new_index = actual_cell[1] * mesh.size + actual_cell[0] + 1
                if self.mesh.m[actual_cell[0] + 1][actual_cell[1]] in good_fields:
                    prevs[new_index] = actual_cell
                    actual_cell = (actual_cell[0] + 1, actual_cell[1])
                    any_selected = True
                else:
                    cells[new_index] = 2

            elif actual_cell[1] < mesh.size - 1 and cells[(actual_cell[1] + 1) * mesh.size + actual_cell[0]] == 0:
                new_index = (actual_cell[1] + 1) * mesh.size + actual_cell[0]
                if self.mesh.m[actual_cell[0]][actual_cell[1] + 1] in good_fields:
                    prevs[new_index] = actual_cell
                    actual_cell = (actual_cell[0], actual_cell[1] + 1)
                    any_selected = True
                else:
                    cells[new_index] = 2

            if not any_selected:  # We go back
                actual_cell = prevs[index]

        return cells[target_cell[1] * mesh.size + target_cell[0]] == 1 # difference 1.