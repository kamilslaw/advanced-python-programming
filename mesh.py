import pygame
import random
import math

from random import randint

size = 13

# blocks types
empty = 0
wall = 1
brick = 2
bomb = 3
bomb_2 = 4
brick_destroyed = 5
# explosion bomb_type
bomb_center = 6
bomb_v = 7
bomb_h = 8
bomb_l = 9
bomb_r = 10
bomb_u = 11
bomb_d = 12

explosion_fields = [bomb_center, bomb_v,
                    bomb_h, bomb_l, bomb_r, bomb_u, bomb_d]

bombs_fields = [bomb, bomb_2]

# directions
none = 0
left = 1
right = 2
up = 3
down = 4

direction_accuracy = 0.5

explosion_time = 3
explosion_size = 4


class Mesh:

    def __init__(self, display, display_settings, load_images, bricks_count):
        self.display = display
        self.display_settings = display_settings
        self.frame = 0
        self.bombs = []
        self.explosions = []
        self.bricks_count = bricks_count

        self.none = none

        self.load_colors()
        if load_images:
            self.load_images()

        self.m = [[wall if x % 2 == 1 and y % 2 == 1 else empty
                   for x in range(size)] for y in range(size)]
        possible_bricks_pos = [(x, y) for x in range(size)
                               for y in range(size) if (x % 2 == 0 or y % 2 == 0) and (x + y > 2) and (x + y < 22)]
        random.shuffle(possible_bricks_pos)
        for (x, y) in possible_bricks_pos[:bricks_count]:
            self.m[x][y] = brick

    def update(self, dt):
        self.update_bombs(dt)
        self.update_explosions(dt)

    def draw(self):
        self.frame += 1
        for (x, row) in enumerate(self.m):
            for (y, el) in enumerate(row):
                self.draw_block(x, y, el)
        if self.frame == 39:
            self.frame = 0
    
    def draw_learning(self):
        for (x, row) in enumerate(self.m):
            for (y, el) in enumerate(row):
                self.draw_block_learning(x, y, el)

    def draw_to_buffer(self, buffer):
        for (x, row) in enumerate(self.m):
            for (y, el) in enumerate(row):
                self.draw_block_to_buffer(x, y, el, buffer)

    def get(self, cell):
        return self.m[cell[0]][cell[1]]

    def shuffle_players(self):
        while True:
            p1_x, p1_y = (random.randrange(size), random.randrange(size))
            if self.m[p1_x][p1_y] is empty:                
                break
        while True:
            p2_x, p2_y = (random.randrange(size), random.randrange(size))
            if (p2_x != p1_x or p2_y != p1_y) and self.m[p2_x][p2_y] is empty:                
                break
        return ((p1_x, p1_y), (p2_x, p2_y))
            
    def put_random_bomb(self, bomber):        
        while True:
            x, y = (random.randrange(size), random.randrange(size))
            if self.m[x][y] is empty:
                self.put_bomb((x, y), bomber)             
                return

    def in_bomb_range(self, sprite):
        for bomb in self.bombs:
            for (x, y) in self.get_bomb_range_fields(bomb):
                if math.sqrt((x - sprite.x) ** 2 + (y - sprite.y) ** 2) < 0.7:
                    return True
        return False

    def in_bomb_range_field(self, sprite):
        cells = []
        in_range = False
        for bomb in self.bombs:
            cells.append((bomb['x'], bomb['y']))
            if math.sqrt((bomb['x'] - sprite.x) ** 2 + (bomb['y'] - sprite.y) ** 2) < 0.9:
                in_range = True
            for (x, y) in self.get_bomb_range_fields(bomb):
                cells.append((x, y))
                if math.sqrt((x - sprite.x) ** 2 + (y - sprite.y) ** 2) < 0.9:
                    in_range = True
                    
        for (x, row) in enumerate(self.m):
            for (y, el) in enumerate(row):
                if el in explosion_fields:
                    cells.append((x, y))
                    if math.sqrt((x - sprite.x) ** 2 + (y - sprite.y) ** 2) < 0.9:
                        in_range = True

        return (in_range, cells)

    def get_bomb_range_fields(self, bomb):
        result = []
        (x, y) = (bomb['x'], bomb['y'])
        e = explosion_size
        for i in range(1, e):
            if x - i >= 0 and self.m[x - i][y] is empty:
                result.append((x - i, y))
            else:
                break
        for i in range(1, e):
            if x + i < size and self.m[x + i][y] is empty:
                result.append((x + i, y))
            else:
                break     
        for i in range(1, e):
            if y - i >= 0 and self.m[x][y - i] is empty:
                result.append((x, y - i))
            else:
                break    
        for i in range(1, e):
            if y + i < size and self.m[x][y + i] is empty:
                result.append((x, y + i))
            else:
                break
        return result

    def update_bombs(self, dt):
        for bomb in self.bombs:
            bomb['time'] += dt
        for bomb in filter(lambda x: x['time'] > explosion_time, self.bombs):
            self.create_explosion(bomb)

    def update_explosions(self, dt):
        for explosion in self.explosions:
            explosion['time'] += dt
        for explosion in filter(lambda x: x['time'] > 0.5, self.explosions):
            self.explosions.remove(explosion)
            self.m[explosion['x']][explosion['y']] = empty

    def put_bomb(self, sprite_pos, bomber):
        (x, y) = sprite_pos
        x = int(x) if abs(int(x) - x) <= 0.5 else int(x) + 1
        y = int(y) if abs(int(y) - y) <= 0.5 else int(y) + 1
        if self.m[x][y] is empty:
            self.m[x][y] = bomber.bomb_type
            self.bombs.append({'bomber': bomber, 'is_ai': bomber.is_black, 'time': 0.0, 'x': x, 'y': y})
            return True
        return False

    def create_explosion(self, bomb):
        self.bombs.remove(bomb)
        bomb['bomber'].release_bomb()
        (x, y) = (bomb['x'], bomb['y'])

        self.m[x][y] = bomb_center
        self.explosions.append({'time': 0.0, 'x': x, 'y': y})

        e = explosion_size
        for i in range(1, e):
            if not self.put_explosion_part(x - i, y, bomb_h if i < e - 1 else bomb_l):
                break
        for i in range(1, e):
            if not self.put_explosion_part(x + i, y, bomb_h if i < e - 1 else bomb_r):
                break
        for i in range(1, e):
            if not self.put_explosion_part(x, y - i, bomb_v if i < e - 1 else bomb_u):
                break
        for i in range(1, e):
            if not self.put_explosion_part(x, y + i, bomb_v if i < e - 1 else bomb_d):
                break

    def put_explosion_part(self, x, y, block):
        if x >= 0 and x < size and y >= 0 and y < size and self.m[x][y] is not wall and self.m[x][y] not in explosion_fields:
            if self.m[x][y] in bombs_fields:
                chain_bomb = list(
                    filter(lambda b: b['x'] == x and b['y'] == y, self.bombs))[0]
                chain_bomb['time'] = explosion_time
                return False
            if self.m[x][y] is brick:
                self.m[x][y] = brick_destroyed
                self.bricks_count -= 1
                self.explosions.append({'time': 0.0, 'x': x, 'y': y})
                return False
            else:
                self.m[x][y] = block
                self.explosions.append({'time': 0.0, 'x': x, 'y': y})
                return True
        else:
            return False

    def get_possible_directions(self, sprite_pos, allowed_fields):
        (x, y) = sprite_pos
        directions = []

        x_index = -1
        if abs(int(x) - x) <= direction_accuracy:
            x_index = int(x)
        elif abs(math.ceil(x) - x) <= direction_accuracy:
            x_index = int(x) + 1

        y_index = -1
        if abs(int(y) - y) <= direction_accuracy:
            y_index = int(y)
        elif abs(math.ceil(y) - y) <= direction_accuracy:
            y_index = int(y) + 1

        if y_index >= 0 and x > 0 and not float(x).is_integer() and self.m[int(x)][y_index] in [empty] + allowed_fields + explosion_fields:
            directions.append(left)
        if y_index >= 0 and x > 1 and float(x).is_integer() and self.m[int(x) - 1][y_index] in [empty] + allowed_fields + explosion_fields:
            directions.append(left)

        if y_index >= 0 and x < size - 1 and self.m[int(x) + 1][y_index] in [empty] + allowed_fields:
            directions.append(right)

        if x_index >= 0 and y > 0 and not float(y).is_integer() and self.m[x_index][int(y)] in [empty] + allowed_fields + explosion_fields:
            directions.append(up)
        if x_index >= 0 and y > 0 and float(y).is_integer() and self.m[x_index][int(y) - 1] in [empty] + allowed_fields + explosion_fields:
            directions.append(up)

        if x_index >= 0 and y < size - 1 and self.m[x_index][int(y) + 1] in [empty] + allowed_fields + explosion_fields:
            directions.append(down)

        return directions

    def update_position(self, sprite, direction, dt, allowed_fields):
        (x, y, speed) = sprite
        ds = speed * dt

        if direction == up or direction == down:
            if abs(int(x) - x) <= direction_accuracy:
                x = int(x)
            else:
                x = math.ceil(x)
            if direction == up:
                y -= ds
                if int(y) < 0 or self.m[x][int(y)] not in [empty] + allowed_fields + explosion_fields:
                    y = math.ceil(y)
            else:
                y += ds
                if math.ceil(y) > size - 1 or self.m[x][math.ceil(y)] not in [empty] + allowed_fields + explosion_fields:
                    y = int(y)

        if direction == left or direction == right:
            if abs(int(y) - y) <= direction_accuracy:
                y = int(y)
            else:
                y = math.ceil(y)
            if direction == left:
                x -= ds
                if int(x) < 0 or self.m[int(x)][y] not in [empty] + allowed_fields + explosion_fields:
                    x = math.ceil(x)
            else:
                x += ds
                if math.ceil(x) > size - 1 or self.m[math.ceil(x)][y] not in [empty] + allowed_fields + explosion_fields:
                    x = int(x)

        if x < 0:
            x = 0
        if x > size - 1:
            x = size - 1
        if y < 0:
            y = 0
        if y > size - 1:
            y = size - 1

        return (x, y)

    def draw_block(self, x, y, el):
        (ratio, padding) = self.display_settings
        self.display.blit(
            self.images[el]
            if el not in [bomb, bomb_2] else self.images[el][self.frame // 20],
            (x * ratio + padding, y * ratio + padding))

    def draw_block_learning(self, x, y, el):
        (ratio, padding) = self.display_settings
        pygame.draw.rect(self.display, self.colors[el],
                         [x * ratio + padding, y * ratio + padding, ratio, ratio], 0)

    def draw_block_to_buffer(self, x, y, el, buffer):
        color = self.colors[el]
        for i in range(8):
            for j in range(8):
                buffer[y * 8 + i][x * 8 + j] = color[0]

    def load_images(self):
        self.images = {
            empty: pygame.image.load('./assets/grass.png').convert_alpha(),
            wall: pygame.image.load('./assets/wall.png').convert_alpha(),
            brick: pygame.image.load('./assets/brick.png').convert_alpha(),
            bomb: [
                pygame.image.load('./assets/bomb_1.png').convert_alpha(),
                pygame.image.load('./assets/bomb_2.png').convert_alpha()],
            brick_destroyed: pygame.image.load('./assets/brick_destroyed.png').convert_alpha(),
            bomb_center: pygame.image.load('./assets/explosion_center.png').convert_alpha(),
            bomb_v: pygame.image.load('./assets/explosion_v.png').convert_alpha(),
            bomb_h: pygame.image.load('./assets/explosion_h.png').convert_alpha(),
            bomb_l: pygame.image.load('./assets/explosion_end_l.png').convert_alpha(),
            bomb_r: pygame.image.load('./assets/explosion_end_r.png').convert_alpha(),
            bomb_u: pygame.image.load('./assets/explosion_end_u.png').convert_alpha(),
            bomb_d: pygame.image.load(
                './assets/explosion_end_d.png').convert_alpha()
        }
        self.images[bomb_2] = self.images[bomb]

    def load_colors(self):
        self.colors = {
            empty: [255, 255, 255],
            wall: [239, 239, 239],
            brick: [188, 188, 188],
            bomb: [93, 93, 93],
            bomb_2: [93, 93, 93],
            brick_destroyed: [188, 188, 188],
            bomb_center: [74, 74, 74],
            bomb_v: [74, 74, 74],
            bomb_h: [74, 74, 74],
            bomb_l: [74, 74, 74],
            bomb_r: [74, 74, 74],
            bomb_u: [74, 74, 74],
            bomb_d: [74, 74, 74]
        }