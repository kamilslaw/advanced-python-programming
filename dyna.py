#!/usr/bin/python

import pygame
#import qlearning as q

from config import Config
from game import Game

import os
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (100, 100)

def main():
    if Config['game']['is_interactive']:
        display = pygame.display.set_mode((Config['game']['width'],Config['game']['height']))
        pygame.display.set_caption(Config['game']['caption'])
        pygame.image.load('./assets/grass.png').convert_alpha()
        game = Game(display)
        game.loop()

    #else:
    #    q.run_learning()


if __name__ == '__main__':
    main()
