Config = {
    'game': {
        'record': False,
        'is_learning_draw_mode': False,
        'caption': 'Dyna blaster',
        'height': 832,
        'width': 832 + 325,
        'fps': 30,
        'is_interactive': True,
        'is_pvp': False,
        'learning_timeout': 2500
    },
    'board': {
        'bricks_count': 17,
        'simple_enemy_count': 3
    },
    'draw': {
        'ratio': 64,
        'padding': 0
    },
    'draw_learning': {
        'ratio': 8,
        'padding': 0
    }
}
