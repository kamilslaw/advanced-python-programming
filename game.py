import pygame
import math
import numpy as np
import time
import datetime
import mesh as mesh
import os

from config import Config
from mesh import Mesh
from player import Player
# from aiPlayer import AiPlayer
from stupidPlayer2 import StupidPlayer2
from simpleEnemy import SimpleEnemy

class GameState:    
    def __init__(self, screen_buffer):
        self.screen_buffer = screen_buffer

class Game:

    def __init__(self, display):
        self.display = display
        self.init_game()

    def loop(self):
        while not self.end:
            events = pygame.event.get()
            self.check_events(events)

            dt = self.clock.get_time() / 1000.0
            

            if (not self.paused):
                self.update(dt, pygame.key.get_pressed())
                self.mesh.update(dt)
                [self.detect_collisions(
                    s1, s2) for s1 in self.sprites for s2 in self.sprites if s1 != s2]
                self.check_explosions()

            self.draw()
            
            self.clock.tick(Config['game']['fps'])

            if Config['game']['record']:
                self.record_game()

        self.draw_end()
        time.sleep(4)

    #### MACHINE LEARNING INTERFACE ####

    def init(self):
        pass

    def new_episode(self): # losuje pozycje i dodaje bomby na plansze
        self.init_game()
        self.shuffle_game()
        self.ticks = 0

    def get_available_buttons_size(self):
        return 5
        
    def get_possible_actions(self):
        left = [1, 0, 0, 0, 0]
        right = [0, 1, 0, 0, 0]
        up = [0, 0, 1, 0, 0]
        down = [0, 0, 0, 1, 0]
        bomb = [0, 0, 0, 0, 1]
        return [left, right, up, down, bomb]

    def get_state(self):
        ratio = Config['draw_learning']['ratio']
        screen_buffer = np.zeros((ratio * mesh.size, ratio * mesh.size), dtype=np.uint8)
        self.mesh.draw_to_buffer(screen_buffer)
        for sprite in self.sprites:
            sprite.draw_to_buffer(screen_buffer)
        return GameState(screen_buffer)
    
    def make_action(self, move):
        self.ticks += 1
        if not self.end:
            dt = 1 / Config['game']['fps']
            keys = [bool(x) for x in move] + [False, False, False, False, False]
            self.update(dt, keys)
            self.mesh.update(dt)
            [self.detect_collisions(
                s1, s2) for s1 in self.sprites for s2 in self.sprites if s1 != s2]
            self.check_explosions()
        return self.get_reward()

    def is_episode_finished(self):
        return self.end # or self.ticks >= Config['game']['learning_timeout']

    ####################################

    def check_events(self, events):
        for event in events:
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                exit()
            elif self.paused and event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                self.paused = False
            elif not self.paused and event.type == pygame.KEYDOWN and event.key == pygame.K_p:
                self.paused = True

    def update(self, dt, keys):
        for sprite in self.sprites:
            sprite.update(dt, keys)

    def draw(self):
        self.display.fill((0, 150, 0))

        pygame.draw.rect(self.display, [240, 240, 240],
                         [Config['game']['height'], 0, Config['game']['width'] - Config['game']['height'], Config['game']['height']], 0)
                         
        if Config['game']['is_learning_draw_mode']:
            self.mesh.draw_learning()
            for sprite in self.sprites:
                sprite.draw_learning()
        else:
            self.mesh.draw()
            for sprite in self.sprites:
                sprite.draw()
                if (isinstance(sprite, StupidPlayer2)):
                    sprite.draw_visualisation()
        
        if self.paused:
            pygame.draw.rect(self.display, [39, 78, 19], [0, 365, Config['game']['height'], 100], 0)
            self.display.blit(self.pause_image, (95, 380))

        pygame.display.flip()

    def draw_end(self):
        if self.is_cpu_dead:
            pygame.draw.rect(self.display, [39, 78, 19], [0, 365, Config['game']['height'], 100], 0)
            self.display.blit(self.win_image, (320, 399))
        else:
            pygame.draw.rect(self.display, [166, 28, 0], [0, 365, Config['game']['height'], 100], 0)
            self.display.blit(self.lost_image, (304, 399))
        pygame.display.flip()

    def detect_collisions(self, sprite_1, sprite_2):
        distance = math.sqrt(
            math.pow(sprite_1.x - sprite_2.x, 2) + math.pow(sprite_1.y - sprite_2.y, 2))
        if distance < 0.7 and (isinstance(sprite_1, Player) or isinstance(sprite_1, StupidPlayer2)) and not isinstance(sprite_2, Player) and not isinstance(sprite_2, StupidPlayer2):
            self.end = True
            self.is_cpu_dead = sprite_1.is_black

    def check_explosions(self):
        for sprite in self.sprites:
            self.check_sprite_explosion(sprite)

    def check_sprite_explosion(self, sprite):
        for (x, row) in enumerate(self.mesh.m):
            for (y, field) in enumerate(row):
                if self.detect_explosion(sprite, x, y, field):
                    return

    def detect_explosion(self, sprite, f_x, f_y, f_type):
        distance = math.sqrt(math.pow(sprite.x - f_x, 2) +
                             math.pow(sprite.y - f_y, 2))
        if distance < 0.7 and f_type in mesh.explosion_fields:
            if isinstance(sprite, Player) or isinstance(sprite, StupidPlayer2):
                self.end = True
                self.is_cpu_dead = sprite.is_black
            else:
                self.sprites.remove(sprite)
            return True
        return False

    def get_reward(self):
        if self.end:
            return 0 if self.is_cpu_dead else 100
        reward = 1
        if self.mesh.in_bomb_range(self.sprites[1]):
            reward -= 30
        for i in range(len(self.sprites) - 2):
            if self.sprites[1].distance_to(self.sprites[2 + i]) < 1:
                reward -= 25
        if self.mesh.in_bomb_range(self.sprites[0]):
            reward += 60

        return reward

    def record_game(self):        
        if self.end:
            self.save_game_states()
        else:
            self.add_game_state()

    def init_game(self):
        self.paused = True
        self.end = False
        self.states = []
        self.width = Config['game']['width']
        self.height = Config['game']['height']

        if Config['game']['is_interactive']:
            self.clock = pygame.time.Clock()

        if Config['game']['is_learning_draw_mode']:
            self.display_settings = (
                Config['draw_learning']['ratio'], Config['draw_learning']['padding'])
        else:
            self.pause_image = pygame.image.load('./assets/pause.png').convert_alpha()
            self.win_image = pygame.image.load('./assets/win.png').convert_alpha()
            self.lost_image = pygame.image.load('./assets/lost.png').convert_alpha()
            self.display_settings = (
                Config['draw']['ratio'], Config['draw']['padding'])

        self.mesh = Mesh(self.display, self.display_settings, not Config['game']['is_learning_draw_mode'],
                         bricks_count=Config['board']['bricks_count'])

        if Config['game']['is_interactive'] and Config['game']['is_pvp']:
            self.sprites = [
                Player(self.display, self.display_settings, self.mesh, mesh.bomb, (0, 0), False, not Config['game']['is_learning_draw_mode'],
                    {'left': pygame.K_LEFT, 'right': pygame.K_RIGHT, 'up': pygame.K_UP, 'down': pygame.K_DOWN, 'bomb': pygame.K_SPACE}),
                Player(self.display, self.display_settings, self.mesh, mesh.bomb_2, (12, 12), True, not Config['game']['is_learning_draw_mode'],
                    {'left': pygame.K_a, 'right': pygame.K_d, 'up': pygame.K_w, 'down': pygame.K_s, 'bomb': pygame.K_LCTRL})
            ]
        elif Config['game']['is_interactive']:
            self.sprites = [
                Player(self.display, self.display_settings, self.mesh, mesh.bomb, (0, 0), False, not Config['game']['is_learning_draw_mode'],
                    {'left': pygame.K_LEFT, 'right': pygame.K_RIGHT, 'up': pygame.K_UP, 'down': pygame.K_DOWN, 'bomb': pygame.K_SPACE}),
                # AiPlayer(self.display, self.display_settings, self.mesh, mesh.bomb_2, (12, 12), True, not Config['game']['is_learning_draw_mode'],
                    #   {'left': 0, 'right': 1, 'up': 2, 'down': 3, 'bomb': 4}, self)
                StupidPlayer2(self.display, self.display_settings, self.mesh, mesh.bomb_2, (12, 12), True, not Config['game']['is_learning_draw_mode'], self)
            ]
        else:
            self.sprites = [
                Player(self.display, self.display_settings, self.mesh, mesh.bomb, (0, 0), False, not Config['game']['is_learning_draw_mode'],
                    {'left': 5, 'right': 6, 'up': 7, 'down': 8, 'bomb': 9}),
                Player(self.display, self.display_settings, self.mesh, mesh.bomb_2, (12, 12), True, not Config['game']['is_learning_draw_mode'],
                    {'left': 0, 'right': 1, 'up': 2, 'down': 3, 'bomb': 4})
            ]

        for _ in range(Config['board']['simple_enemy_count']):
            self.sprites.append(
                SimpleEnemy(self.display, self.display_settings, self.mesh, not Config['game']['is_learning_draw_mode']))

    def shuffle_game(self):
        ((self.sprites[0].x, self.sprites[0].y), (self.sprites[1].x,
                                                  self.sprites[1].y)) = self.mesh.shuffle_players()
        for _ in range(3):
           self.mesh.put_random_bomb(self.sprites[0])

    def add_game_state(self):
        if (self.sprites[0].direction != mesh.none or self.sprites[0].bomb_planted):
            state = {
                'player_x': "{:.2f}".format(self.sprites[0].x),
                'player_y': "{:.2f}".format(self.sprites[0].y),
                'allowed_bombs': self.sprites[0].allowed_bombs,
                'opponent_x': "{:.2f}".format(self.sprites[1].x),
                'opponent_y': "{:.2f}".format(self.sprites[1].y)
            }
            for i in range(Config['board']['simple_enemy_count']):
                enemy_alive = len(self.sprites) > 2 + i
                state['enemy' + str(i) + '_is_alive'] = 1 if enemy_alive else 0
                if enemy_alive:
                    state['enemy' +
                          str(i) + '_x'] = "{:.2f}".format(self.sprites[2 + i].x)
                    state['enemy' +
                          str(i) + '_y'] = "{:.2f}".format(self.sprites[2 + i].y)
                else:
                    state['enemy' + str(i) + '_x'] = -1
                    state['enemy' + str(i) + '_y'] = -1
            for (x, row) in enumerate(self.mesh.m):
                for (y, field) in enumerate(row):
                    if field in mesh.bombs_fields:
                        state[str(x) + '_' + str(y)] = mesh.bomb
                    elif field in mesh.explosion_fields:
                        state[str(x) + '_' + str(y)] = mesh.bomb_center
                    else:
                        state[str(x) + '_' + str(y)] = field

            if self.sprites[0].bomb_planted:
                state['output'] = 10
            elif self.sprites[0].direction == mesh.left:
                state['output'] = 1
            elif self.sprites[0].direction == mesh.right:
                state['output'] = 2
            elif self.sprites[0].direction == mesh.up:
                state['output'] = 3
            elif self.sprites[0].direction == mesh.down:
                state['output'] = 4

            self.states.append(state)

    def save_game_states(self):
        if not os.path.exists('data'):
            os.makedirs('data')
        with open(
                'data/' + str(datetime.datetime.now()).replace(':', '.') + ".csv", 'w') as file:
            header = ','.join([key for key in self.states[0]])
            file.write(header + '\n')
            for state in self.states:
                file.write(','.join([str(state[key]) for key in state]) + '\n')
